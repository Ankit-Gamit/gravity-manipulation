using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region VARIABLES
    [Header("Player Movement")]
    public Transform player;
    public CharacterController characterController;
    public float playerSpeed = 1.9f;
    public Transform playerCamera;

    [Header("Player Animator and Gravity")]
    public float gravity = -9.81f;
    public Animator playerAnimator;

    [Header("Player Jumping and Velocity")]
    public float turnCalmTime = 0.1f;
    private float _turnCalmVelocity;
    public float jumpRange = 1f;
    public Transform surfaceCheck;
    private bool _onSurface;
    public float surfaceDistance = 0.4f;
    public LayerMask surfaceMask;

    private float _gravityStrength = 9.8f;
    private Vector3 _gravityDirection = Vector3.down;

    public GameObject hologramPrefab;
    private GameObject hologramInstance;
    public float hologramOffset = 1f;
    private float _playerRotationZ = 0f;

    public bool isGameRunning { get; set; }
    #endregion

    public void ResetGravity()
    {
        _gravityDirection = Vector3.down;
    }

    private void Update()
    {
        if (!isGameRunning) return;

        _onSurface = Physics.CheckSphere(surfaceCheck.position, surfaceDistance, surfaceMask);

        PlayerMove();
        PlayerJump();
    }

    private void PlayerMove()
    {
        _gravityDirection = GetGravityDirectionInput();

        // Apply custom gravity
        characterController.Move(_gravityDirection * _gravityStrength * Time.deltaTime);

        float horizontal_axis = Input.GetAxisRaw("Horizontal");
        float vertical_axis = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal_axis, 0f, vertical_axis).normalized;

        if (direction.magnitude >= 0.1f)
        {
            playerAnimator.SetBool("Idle", false);
            playerAnimator.SetBool("Running", true);

            float targetAngle = (float)Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + playerCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref _turnCalmVelocity, turnCalmTime);

            transform.rotation = Quaternion.Euler(0f, angle, _playerRotationZ);

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            characterController.Move(moveDirection.normalized * playerSpeed * Time.deltaTime);
        }
        else
        {
            playerAnimator.SetBool("Idle", true);
            playerAnimator.SetBool("Running", false);
            playerAnimator.SetBool("Falling", false);
        }
    }

    private Vector3 GetGravityDirectionInput()
    {
        Vector3 newGravityDirection = _gravityDirection;

        // Check for arrow key presses and update gravity direction accordingly
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            RotatePlayer(-180f);
            newGravityDirection = Vector3.up;
            // SetGravityDirection(Vector3.up);
            InstantiateHologram(newGravityDirection);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            RotatePlayer(0f);
            newGravityDirection = Vector3.down;
            // SetGravityDirection(Vector3.down);
            InstantiateHologram(newGravityDirection);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            RotatePlayer(90f);
            newGravityDirection = Vector3.left;
            // SetGravityDirection(Vector3.left);
            InstantiateHologram(newGravityDirection);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            RotatePlayer(-90f);
            newGravityDirection = Vector3.right;
            // SetGravityDirection(Vector3.right);
            InstantiateHologram(newGravityDirection);
        }

        newGravityDirection.Normalize();
        return newGravityDirection;
    }

    private void RotatePlayer(float rotationZ)
    {
        _playerRotationZ = rotationZ;
        player.transform.Rotate(new Vector3(0f, 0f, rotationZ));
    }

    // private void SetGravityDirection(Vector3 newGravityDirection)
    // {
    //     newGravityDirection.Normalize();

    //     _gravityDirection = newGravityDirection;

    //     InstantiateHologram(newGravityDirection);
    // }

    void InstantiateHologram(Vector3 direction)
    {
        // Destroy the existing hologram if it exists
        if (hologramInstance != null)
        {
            Destroy(hologramInstance);
        }

        // Instantiate the hologram prefab with position in the corresponding direction
        hologramInstance = Instantiate(hologramPrefab, transform.position + direction * hologramOffset, Quaternion.Euler(0f, 0f, _playerRotationZ));

    }

    private void PlayerJump()
    {
        if (Input.GetButton("Jump") && _onSurface)
        {
            playerAnimator.SetBool("Idle", false);
            playerAnimator.SetBool("Falling", true);
            transform.Translate(Vector3.up * jumpRange);
        }
        else
        {
            playerAnimator.SetBool("Idle", true);
            playerAnimator.SetBool("Falling", false);
        }
    }
}
