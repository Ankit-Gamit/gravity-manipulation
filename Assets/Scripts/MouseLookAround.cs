using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLookAround : MonoBehaviour
{
    public Transform player;

    private void Update()
    {
        transform.LookAt(player, Vector3.up);
    }
}
