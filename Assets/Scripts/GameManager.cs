using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [HideInInspector] public int score = 0;
    [SerializeField] private Text _timerText;
    [SerializeField] private Text _scoreText;

    public GameObject GameOverScreen;
    public Text gameOverText;

    public List<GameObject> pointCubes;

    private string _gameOverText;

    public PlayerController playerController;

    public static GameManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        playerController.isGameRunning = false;
        // Cursor.lockState = CursorLockMode.Locked;
    }

    public void StartGame()
    {
        foreach (var cube in pointCubes)
        {
            cube.gameObject.SetActive(true);
        }
        score = 0;
        playerController.ResetGravity();
        playerController.isGameRunning = true;

        Time.timeScale = 1;

        StartCoroutine(nameof(StartTimer));
    }

    private IEnumerator StartTimer()
    {
        var totalTime = 120;

        for (int i = 0; i < 120; i++)
        {
            if (totalTime > 0)
            {
                totalTime -= 1;
                int minutes = Mathf.FloorToInt(totalTime / 60);
                int seconds = Mathf.FloorToInt(totalTime % 60);
                _timerText.text = $"{minutes:D2}:{seconds:D2}";
            }
            yield return new WaitForSeconds(1f);
        }

        _gameOverText = "You lose!";
        GameOver();
    }

    public void StopGame()
    {
        _gameOverText = "You dies!";
        GameOver();
    }

    private void GameOver()
    {
        Debug.Log("Game Over");
        Time.timeScale = 0;
        playerController.isGameRunning = false;
        GameOverScreen.gameObject.SetActive(true);
        gameOverText.text = _gameOverText;
    }

    public void UpdateScore()
    {
        score += 1;
        _scoreText.text = "Score: " + score;

        if (score == 5)
        {
            StopCoroutine(nameof(StartTimer));
            _gameOverText = "You Win!!!";
            GameOver();
        }
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#endif

        Application.Quit();
    }
}
